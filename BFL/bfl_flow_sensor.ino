/**********************************************************
This is example code for using the Adafruit liquid flow meters. 

5/9/2017  Modified by  Fred Bliss for use as a laser waterflow interlock  

Pin 2 is used as the input from the flow sensor
Pin 4 is used to drive a SainSmart relay module

Connect the waterflow sensor red wire to arduino +5V, 
                             black wire to arduino Gnv
                             yellow wire to arduino digital pin #2
    
Connect a wire from the arduino +5v to realay VCC pin
                                Gnd to relay GND
                                pin 4 to relay IN1
Connect the relay output N.O. terminals in series with the laser power supply WP input and laser power supply ground.
The laser will be powered when the WP input is grounded.  
The circuit works so that waterflow detected by the sensor will apply a LOW signal to IN1 of the relay module.
Since the relay is active low, this will close the ouput contacts and allow the laser to fire. 
If waterflow rate falls below a specificed level, a HIGH signal will be applied to IN1 and the relay will open and disable 
the laser power.



Adafruit invests time and resources providing this open source code, 
please support Adafruit and open-source hardware by purchasing 
products from Adafruit!

Written by Limor Fried/Ladyada  for Adafruit Industries.  
BSD license, check license.txt for more information
All text above must be included in any redistribution
**********************************************************/


// which pin to use for reading the sensor? can use any pin!
#define FLOWSENSORPIN 2
// which pin to use to drive the relay
#define RELAYPIN 4
 
// count how many pulses!
volatile uint16_t pulses = 0;
// track the state of the pulse pin
volatile uint8_t lastflowpinstate;
// you can try to keep time of how long it is between pulses
volatile uint32_t lastflowratetimer = 0;
// and use that to calculate a flow rate
volatile float flowrate;
// Interrupt is called once a millisecond, looks for any pulses from the sensor!
SIGNAL(TIMER0_COMPA_vect) {
  uint8_t x = digitalRead(FLOWSENSORPIN);
  
  if (x == lastflowpinstate) {
    lastflowratetimer++;
    return; // nothing changed!
  }
  
  if (x == HIGH) {
    //low to high transition!
    pulses++;
  }
  lastflowpinstate = x;
  flowrate = 1000.0;
  flowrate /= lastflowratetimer;  // in hertz
  lastflowratetimer = 0;
}

void useInterrupt(boolean v) {
  if (v) {
    // Timer0 is already used for millis() - we'll just interrupt somewhere
    // in the middle and call the "Compare A" function above
    OCR0A = 0xAF;
    TIMSK0 |= _BV(OCIE0A);
  } else {
    // do not call the interrupt function COMPA anymore
    TIMSK0 &= ~_BV(OCIE0A);
  }
}

void setup() {
   Serial.begin(9600);
   Serial.print("Flow sensor test!");

   pinMode(LED_BUILTIN, OUTPUT);
   digitalWrite(LED_BUILTIN,LOW);

   pinMode(RELAYPIN, OUTPUT);
   digitalWrite(RELAYPIN, HIGH);
   
   
   pinMode(FLOWSENSORPIN, INPUT);
   digitalWrite(FLOWSENSORPIN, HIGH);
   lastflowpinstate = digitalRead(FLOWSENSORPIN);
   useInterrupt(true);
}

void loop()                     // run over and over again
{ 
  float rate = flowrate / 7.5;
  Serial.print("Freq: "); Serial.println(flowrate);
  Serial.print("Pulses: "); Serial.println(pulses, DEC);
  Serial.print("Flow l/min: "); Serial.println(rate);
  if (flowrate > 10) {
    digitalWrite(LED_BUILTIN,HIGH); //turn on the built-in led
    digitalWrite(RELAYPIN, LOW);  //close the relay to enable the laser power supply
  }
  else {
        digitalWrite(LED_BUILTIN,LOW); //turn off the built-in led
        digitalWrite(RELAYPIN, HIGH);  //open the relay to disable the laser power supply
  }
  
  // if a plastic sensor use the following calculation
  // Sensor Frequency (Hz) = 7.5 * Q (Liters/min)
  // Liters = Q * time elapsed (seconds) / 60 (seconds/minute)
  // Liters = (Frequency (Pulses/second) / 7.5) * time elapsed (seconds) / 60
  // Liters = Pulses / (7.5 * 60)
  float liters = pulses;
  liters /= 7.5;
  liters /= 60.0;


/*
  // if a brass sensor use the following calculation
  float liters = pulses;
  liters /= 8.1;
  liters -= 6;
  liters /= 60.0;
*/
  Serial.print(liters); Serial.println(" Liters");
 
  delay(100);
}
